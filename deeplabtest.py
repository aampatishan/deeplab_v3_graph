import cv2
import numpy as np
import tensorflow as tf
image = cv2.imread('1.jpg')

import keras
from keras_applications import mobilenet_v2

MobileNetV2 = mobilenet_v2.MobileNetV2
decode_predictions = mobilenet_v2.decode_predictions
preprocess_input = mobilenet_v2.preprocess_input

image = image.astype(float)
image = preprocess_input(image)

def load_graph(frozen_graph_filename):
    
    with tf.gfile.GFile(frozen_graph_filename, "rb") as f:
        graph_def = tf.GraphDef()
        graph_def.ParseFromString(f.read())    
    with tf.Graph().as_default() as graph:       
        tf.import_graph_def(graph_def, name="prefix")
    return graph
    
graph = load_graph('deeplabv3_mnv2_pascal_train_aug/frozen_inference_graph.pb')

x = graph.get_tensor_by_name('prefix/ImageTensor:0')
y = graph.get_tensor_by_name('prefix/SemanticPredictions:0')

with tf.Session(graph=graph) as sess:
        y_out = sess.run(y, feed_dict={
            x: [image] 
})